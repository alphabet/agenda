/* eslint-disable import/prefer-default-export */
import { Meteor } from 'meteor/meteor';
import i18n from 'meteor/universe:i18n';
import SimpleSchema from 'simpl-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';

import { getLabel } from '../utils/functions';

export const getIdToken = new ValidatedMethod({
  name: 'users.getIdToken',
  validate: null,
  run() {
    if (!this.userId) {
      throw new Meteor.Error('api.users.getIdToken.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }
    // check user existence
    const user = Meteor.users.findOne({ _id: this.userId });
    if (user === undefined) {
      throw new Meteor.Error('api.users.getIdToken.unknownUser', i18n.__('api.users.unknownUser'));
    }
    return user.services?.keycloak?.idToken || null;
  },
});

export const setLogoutType = new ValidatedMethod({
  name: 'users.setLogoutType',
  validate: new SimpleSchema({
    logoutType: { type: String, label: getLabel('api.users.labels.logoutType') },
  }).validator(),

  run({ logoutType }) {
    if (!this.userId) {
      throw new Meteor.Error('api.users.setLogoutType.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }
    Meteor.users.update(this.userId, {
      $set: { logoutType },
    });
  },
});
