import { Meteor } from 'meteor/meteor';
import i18n from 'meteor/universe:i18n';
import RegEx from './regExp';

export function isActive(userId) {
  if (!userId) return false;
  const user = Meteor.users.findOne(userId, { fields: { isActive: 1 } });
  if (user.isActive === true) return true;
  return false;
}

export function getLang() {
  return (
    (navigator.languages && navigator.languages[0]) ||
    navigator.language ||
    navigator.browserLanguage ||
    navigator.userLanguage ||
    'en-US'
  );
}

export function getLabel(i18nLabel) {
  return () => i18n.__(i18nLabel);
}

export function registerSchemaMessages() {
  const regExpMessages = [
    { exp: RegEx.Email, msg: 'SimpleSchema.RegEx.Email' },
    { exp: RegEx.EmailWithTLD, msg: 'SimpleSchema.RegEx.EmailWithTLD' },
    { exp: RegEx.Domain, msg: 'SimpleSchema.RegEx.Domain' },
    { exp: RegEx.WeakDomain, msg: 'SimpleSchema.RegEx.WeakDomain' },
    { exp: RegEx.IP, msg: 'SimpleSchema.RegEx.IP' },
    { exp: RegEx.IPv4, msg: 'SimpleSchema.RegEx.IPv4' },
    { exp: RegEx.IPv6, msg: 'SimpleSchema.RegEx.IPv6' },
    { exp: RegEx.Url, msg: 'SimpleSchema.RegEx.Url' },
    { exp: RegEx.Id, msg: 'SimpleSchema.RegEx.Id' },
    { exp: RegEx.ZipCode, msg: 'SimpleSchema.RegEx.ZipCode' },
    { exp: RegEx.Phone, msg: 'SimpleSchema.RegEx.Phone' },
  ];
  const customMessages = {
    required: (ctx, label) => i18n.__('SimpleSchema.required', { ...ctx, label }),
    minString: (ctx, label) => i18n.__('SimpleSchema.minString', { ...ctx, label }),
    maxString: (ctx, label) => i18n.__('SimpleSchema.maxString', { ...ctx, label }),
    minNumber: (ctx, label) => i18n.__('SimpleSchema.minNumber', { ...ctx, label }),
    maxNumber: (ctx, label) => i18n.__('SimpleSchema.maxNumber', { ...ctx, label }),
    minNumberExclusive: (ctx, label) => i18n.__('SimpleSchema.minNumberExclusive', { ...ctx, label }),
    maxNumberExclusive: (ctx, label) => i18n.__('SimpleSchema.maxNumberExclusive', { ...ctx, label }),
    minDate: (ctx, label) => i18n.__('SimpleSchema.minDate', { ...ctx, label }),
    maxDate: (ctx, label) => i18n.__('SimpleSchema.maxDate', { ...ctx, label }),
    badDate: (ctx, label) => i18n.__('SimpleSchema.badDate', { ...ctx, label }),
    minCount: (ctx, label) => i18n.__('SimpleSchema.minCount', { ...ctx, label }),
    maxCount: (ctx, label) => i18n.__('SimpleSchema.maxCount', { ...ctx, label }),
    noDecimal: (ctx, label) => i18n.__('SimpleSchema.noDecimal', { ...ctx, label }),
    notAllowed: (ctx, label) => i18n.__('SimpleSchema.notAllowed', { ...ctx, label }),
    expectedType: (ctx, label) => {
      const finalCtx = { ...ctx, label };
      const i18nEntry = `SimpleSchema.dataTypes.${ctx.dataType}`;
      const typeTranslated = i18n.__(i18nEntry);
      if (typeTranslated !== i18nEntry) {
        // translatation for type is available
        finalCtx.dataType = typeTranslated;
      }
      return i18n.__('SimpleSchema.expectedType', finalCtx);
    },
    keyNotInSchema: (ctx, label) => i18n.__('SimpleSchema.keyNotInSchema', { ...ctx, label }),
    regEx: (ctx, label) => {
      // See if there's one where exp matches this expression
      let msgObj;
      if (ctx.regExp) {
        msgObj = regExpMessages.find((o) => o.exp && o.exp.toString() === ctx.regExp);
      }
      const regExpMessage = msgObj ? i18n.__(msgObj.msg) : i18n.__('SimpleSchema.RegEx.Default');
      return `${label} ${regExpMessage}`;
    },
  };
  globalThis.simpleSchemaGlobalConfig = {
    getErrorMessage(error, label) {
      if (typeof customMessages[error.type] === 'function') {
        return customMessages[error.type](error, label);
      }
      return undefined;
    },
  };
}

export const validateEmail = (email) => {
  const re =
    /* eslint-disable-next-line max-len */
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

export const getGroupName = (group) => {
  if (group.type !== 15) return group.name;

  return `[STRUC] ${group.name.slice(group.name.indexOf('_') + 1, group.name.length)}`;
};

const regValidateStrict = /[<>"'&]/g;
const regValidate = /((<|%3C|&lt;)script)|(('|"|%22|%27) *on[a-z_]+ *(=|%3D))/gi;

/** Check a string for malicious content */
export const validateString = (content, strict = false) => {
  if (content.length > 50000) {
    throw new Meteor.Error('api.utils.functions.validateString.tooLong', i18n.__('api.utils.stringTooLong'));
  }
  /** strict forbids any of the following characters : < > " ' &
      otherwise, forbid script tags and pattern like " onload=... */
  const scriptRegex = strict ? regValidateStrict : regValidate;
  if (content.match(scriptRegex) !== null) {
    throw new Meteor.Error(
      'api.utils.functions.validateString.error',
      i18n.__(strict ? 'api.utils.badCharsDetected' : 'api.utils.scriptDetected'),
    );
  }
  return content;
};
