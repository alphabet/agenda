import { Meteor } from 'meteor/meteor';
import ContextSettings from '../contextsettings';

const getTheme = Meteor.methods({
  'contextsettings.getTheme': () => {
    return ContextSettings.find({ key: 'public.theme' }).fetch();
  },
});

export default getTheme;
