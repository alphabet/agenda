import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from 'tss-react/mui';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';

import { useAppContext } from '../../contexts/context';

const Transition = React.forwardRef((props, ref) => <Slide direction="up" ref={ref} {...props} />);

const useStyles = (isMobile) =>
  makeStyles()(() => ({
    modal: {
      '& .MuiDialog-paperWidthSm': {
        minWidth: isMobile ? '95%' : 800,
        maxWidth: '70%',
        minHeight: '50%',
      },
    },
  }));

const ModalWrapper = ({ open, onClose, title, children, loading, buttons = [] }) => {
  const [{ isMobile }] = useAppContext();
  const { classes } = useStyles(isMobile)();

  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      className={classes.modal}
      onClose={onClose}
      aria-labelledby="alert-dialog-slide-title"
      aria-describedby="alert-dialog-slide-description"
    >
      <DialogTitle id="alert-dialog-slide-title">{title}</DialogTitle>
      <DialogContent>{children}</DialogContent>
      <DialogActions sx={{ justifyContent: 'center' }}>
        {buttons.map((b) => {
          let finalDisabled = b.disabled;
          if (loading !== undefined) {
            if (b.disabled !== undefined) {
              finalDisabled = loading || b.disabled;
            } else {
              finalDisabled = loading;
            }
          }
          return (
            !!b.text && (
              <Button key={b.key} color={b.color} disabled={finalDisabled} variant="contained" onClick={b.onClick}>
                {b.text}
              </Button>
            )
          );
        })}
      </DialogActions>
    </Dialog>
  );
};

ModalWrapper.propTypes = {
  open: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  onClose: PropTypes.func.isRequired,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.any), PropTypes.objectOf(PropTypes.any)]).isRequired,
  loading: PropTypes.bool,
  buttons: PropTypes.arrayOf(PropTypes.any).isRequired,
};

ModalWrapper.defaultProps = {
  loading: false,
};

export default ModalWrapper;
