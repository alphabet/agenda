import { makeStyles } from 'tss-react/mui';

const useStyles = makeStyles()((theme, { isMobile }) => ({
  hidden: {
    display: 'none',
  },
  container: {
    padding: '80px 15px 15px 15px',
    '& button.fc-button': {
      backgroundColor: theme.palette.primary.main,
      borderColor: 'transparent',
    },
    '& button.fc-button.fc-button-active,button.fc-button:hover': {
      backgroundColor: theme.palette.primary.dark,
    },
    '& button.fc-button:disabled': {
      backgroundColor: theme.palette.primary.dark,
    },
    '& .fc-toolbar': {
      flexDirection: isMobile ? 'column-reverse' : 'row',
    },
    '& .fc-toolbar-chunk': {
      width: isMobile ? '100%' : 'auto',
      textAlign: 'center',
      marginTop: 5,
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      '& > .fc-button-group': {
        marginBottom: 5,
        marginLeft: '0rem !important',
      },
      '& > .fc-button': {
        width: '100%',
        marginLeft: '0rem !important',
      },
    },
  },
}));

const useCalendarStyles = (isMobile) => {
  return useStyles({ isMobile });
};

export default useCalendarStyles;
