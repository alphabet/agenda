import React from 'react';
import { useTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import { makeStyles } from 'tss-react/mui';
import Chip from '@mui/material/Chip';
import Avatar from '@mui/material/Avatar';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import DeleteButton from '@mui/icons-material/CloseRounded';
import DoneIcon from '@mui/icons-material/Done';
import RemoveIcon from '@mui/icons-material/CloseOutlined';
import Groups from '../../../api/groups/groups';
import { getGroupName } from '../../../api/utils/functions';

const useStyles = makeStyles()((theme) => ({
  chip: {
    margin: 5,
  },
  successChip: {
    backgroundColor: theme.palette.success.main,
    '&:focus': {
      backgroundColor: theme.palette.success.dark,
    },
  },
  errorChip: {
    backgroundColor: theme.palette.error.main,
    '&:focus': {
      backgroundColor: theme.palette.error.dark,
    },
  },
  delete: {
    marginRight: 10,
  },
  header: {
    display: 'flex',
    alignItem: 'center',
  },
  results: {
    flexWrap: 'wrap',
  },
}));

const SingleGroupDisplay = ({ group, handleDelete, view, event }) => {
  const { classes } = useStyles();

  const users = useTracker(() => {
    const { animators = [], members = [] } = Groups.findOne({ _id: group._id }) || {};
    return Meteor.users.find({ _id: { $in: [...animators, ...members] } }).fetch();
  });

  return (
    <Accordion>
      <AccordionSummary className={classes.header} square="true" expandIcon={<ExpandMoreIcon />}>
        {handleDelete && (
          <IconButton
            size="small"
            onClick={() => handleDelete(group._id)}
            color="primary"
            aria-label="remove group"
            component="span"
            className={classes.delete}
          >
            <DeleteButton />
          </IconButton>
        )}
        <Typography>{getGroupName(group)}</Typography>
      </AccordionSummary>
      <AccordionDetails className={classes.results}>
        {users.map(({ emails = [], avatar, _id }) => {
          const status = () => {
            if (event.participants) {
              const thisUser = event.participants.find((p) => p._id === _id);
              return thisUser ? thisUser.status : 0;
            }
            return 1;
          };
          const displayStatus = view && event.participants && status() !== 1;
          return (
            <Chip
              key={emails[0].address}
              avatar={
                <Avatar
                  alt={emails[0].address}
                  src={avatar}
                  style={{ backgroundColor: avatar ? 'rgba(255,255,255,0.7)' : '' }}
                />
              }
              label={emails[0].address}
              className={`${classes.chip} ${
                displayStatus ? (status() === 0 ? classes.errorChip : classes.successChip) : ''
              }`}
              color={displayStatus ? 'primary' : 'default'}
              onDelete={displayStatus ? () => null : null}
              deleteIcon={displayStatus ? status() === 0 ? <RemoveIcon /> : <DoneIcon /> : null}
            />
          );
        })}
      </AccordionDetails>
    </Accordion>
  );
};

export default SingleGroupDisplay;

SingleGroupDisplay.propTypes = {
  group: PropTypes.objectOf(PropTypes.any).isRequired,
  handleDelete: PropTypes.func,
  view: PropTypes.bool,
  event: PropTypes.objectOf(PropTypes.any).isRequired,
};

SingleGroupDisplay.defaultProps = {
  handleDelete: null,
  view: false,
};
