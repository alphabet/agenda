import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { makeStyles } from 'tss-react/mui';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import MenuBar from './MenuBar';
import MainMenu from './MainMenu';
import { useAppContext } from '../../contexts/context';
// import { ThemeContext } from '../../contexts/colorModeContext/provider';
import themeLogo from '../../utils/themeLogo';

const useStyles = makeStyles()((theme) => ({
  appbar: {
    backgroundColor: theme.palette.tertiary.main,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    minHeight: 48,
  },
  imgLogo: {
    maxHeight: '40px',
    outline: 'none',
    display: 'flex',
  },
  grow: {
    flexGrow: 1,
  },
  toolbar: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  rightContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItem: 'center',
    height: 48,
  },
  appTitle: {
    fontSize: '30px',
    fontFamily: 'OdinRounded !important',
    color: '#372f84',
    alignSelf: 'center',
  },
  imageApp: {
    alignSelf: 'center',
    maxHeight: '40px',
    height: '40px',
    paddingLeft: '16px',
    paddingRight: '0.5vw',
  },
}));

function TopBar() {
  const [{ isMobile, user }] = useAppContext();
  const [theme, setTheme] = useState('eole');
  // const { switchColorMode } = useContext(ThemeContext);
  const { classes } = useStyles();

  useEffect(() => {
    Meteor.call('contextsettings.getTheme', (e, r) => {
      if (e) {
        console.log(e.message);
      } else {
        setTheme(r[0].value[0]);
      }
    });
  }, []);

  console.log(theme);

  return (
    <AppBar position="fixed">
      <Toolbar className={classes.appbar}>
        <Link to="/" className={classes.imgLogo}>
          <img src={themeLogo(theme)} alt="App logo" className={classes.imageApp} height="40" />
          {!isMobile && <h1 className={classes.appTitle}>Agenda</h1>}
        </Link>

        {!isMobile && !!user && <MenuBar />}
        {!!user && (
          <div className={classes.rightContainer}>
            <MainMenu user={user} />
          </div>
        )}
      </Toolbar>
    </AppBar>
  );
}

export default TopBar;
