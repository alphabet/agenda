import React, { Suspense, lazy, useState, useEffect } from 'react';
import { Accounts } from 'meteor/accounts-base';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { MatomoProvider, useMatomo } from '@datapunt/matomo-tracker-react';
import CssBaseline from '@mui/material/CssBaseline';
import Spinner from '../components/system/Spinner';
import MsgHandler from '../components/system/MsgHandler';
import DynamicStore from '../contexts/context';
import instance from '../utils/matomo';
import { ThemeContextProvider } from '../contexts/colorModeContext/provider';
import themeLogo from '../utils/themeLogo';

// dynamic imports
const MainLayout = lazy(() => import('./MainLayout'));

function App() {
  const [userFailed, setUserFailed] = useState(false);
  const [theme, setTheme] = useState('eole');
  const stopCallback = Accounts.onLoginFailure((details) => {
    if (details.error.error === 'api.users.createUser') {
      setUserFailed(true);
    }
  });
  const { trackPageView, enableLinkTracking } = useMatomo();
  enableLinkTracking();

  useEffect(() => {
    Meteor.call('contextsettings.getTheme', (e, r) => {
      if (e) {
        console.log(e.message);
      } else {
        setTheme(r[0].value[0]);
      }
    });
    trackPageView();
    return () => {
      if (typeof stopCallback.stop === 'function') {
        stopCallback.stop();
      }
    };
  }, []);

  const linkElement = document.querySelector('link[rel=icon]');
  linkElement.href = themeLogo(theme);

  return (
    <>
      <CssBaseline />
      <Suspense fallback={<Spinner full />}>
        <Switch>
          <Route path="/" component={MainLayout}>
            <MainLayout userFailed={userFailed} setUserFailed={setUserFailed} />
          </Route>
        </Switch>
      </Suspense>
      <MsgHandler />
    </>
  );
}

export default () => (
  <MatomoProvider value={instance}>
    <ThemeContextProvider>
      <BrowserRouter>
        <DynamicStore>
          <App />
        </DynamicStore>
      </BrowserRouter>
    </ThemeContextProvider>
  </MatomoProvider>
);
