import { useMemo } from 'react';
import { createTheme } from '@mui/material/styles';
import getLaboiteTheme from './laboite/palette';

function appTheme(mode) {
  const allThemes = {
    laboite: getLaboiteTheme(mode),
  };
  let selectedTheme = Meteor.settings.public.theme;
  // if theme in settings doesn't exist, set eole theme to default theme
  if (!Object.keys(allThemes).includes(selectedTheme)) selectedTheme = 'laboite';
  const theme = useMemo(() => createTheme(allThemes[selectedTheme]), [mode]);
  return theme;
}

export default appTheme;
