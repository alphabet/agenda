import getCommons from './commons';

function getLaboiteTheme(mode) {
  const palette = {
    mode,
    ...(mode === 'light'
      ? {
          primary: {
            main: '#011CAA',
            light: '#ECEEF8',
            dark: '#212F74',
          },
          secondary: {
            main: '#E48231',
            light: '#FFDBA5',
          },
          tertiary: {
            main: '#fff',
          },
          backgroundFocus: {
            main: '#ffe0b2',
          },
          text: {
            primary: '#040D3E',
            green: '#27a658',
          },
          background: {
            default: '#F9F9FD',
            inputs: '#F9F9FD',
          },
          info: {
            main: '#518fff',
            light: '#85acff',
            dark: '#9ebdff',
          },
          success: {
            main: '#18753c',
            light: '#2aac5c',
            dark: '#249851',
          },
          warning: {
            main: '#b34000',
            light: '#983600',
            dark: '#842f00',
          },
          error: {
            main: '#ce0500',
            light: '#b20400',
            dark: '#9c0400',
          },
        }
      : {
          primary: {
            main: '#9ebdff',
            light: '#5302AB',
            dark: '#454B55',
          },
          secondary: {
            main: '#E48231',
            light: '#FFDBA5',
            dark: '#E36130',
          },

          tertiary: {
            main: '#161616',
          },
          backgroundFocus: {
            main: '#313178',
          },
          text: {
            primary: '#ffffff',
            green: '#45b870',
          },
          background: {
            default: '#161616',
            inputs: '#454B55',
            paper: '#383838',
          },
          info: {
            main: '#518fff',
            light: '#85acff',
            dark: '#9ebdff',
          },
          success: {
            main: '#18753c',
            light: '#2aac5c',
            dark: '#249851',
          },
          warning: {
            main: '#b34000',
            light: '#983600',
            dark: '#842f00',
          },
          error: {
            main: '#ce0500',
            light: '#b20400',
            dark: '#9c0400',
          },
        }),
  };
  const COMMONS = getCommons();
  const laboite = {
    ...COMMONS,
    palette,
  };

  return laboite;
}

export default getLaboiteTheme;
