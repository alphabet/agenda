function getCommons() {
  const COMMONS = {
    shape: {
      borderRadius: 8,
    },
    typography: {
      fontFamily: 'WorkSansRegular',
      h1: {
        fontFamily: 'WorkSansBold',
      },
      h2: {
        fontFamily: 'WorkSansBold',
      },
      h3: {
        fontFamily: 'WorkSansBold',
      },
      h4: {
        fontFamily: 'WorkSansBold',
      },
      h5: {
        fontFamily: 'WorkSansBold',
      },
      h6: {
        fontFamily: 'WorkSansBold',
      },
    },
  };
  return COMMONS;
}

export default getCommons;
