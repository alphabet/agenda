export default function themeLogo(theme) {
  switch (theme) {
    case 'laboite':
      return '/images/Logo-A.svg';
    case 'eole':
      return '/images/puce_eole.png';
    case 'mim':
      return '/images/logo-9.svg';
    default:
      return '/images/puce_eole.png';
  }
}
